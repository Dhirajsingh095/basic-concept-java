package BasicOops;
/// Aggregation Example (hash a relation Ship)
class Square {

	public double square(double n) {
		return n * n;
	}

}


class Circle{
	public double circleArea(double radious) {
		Square sq;
		sq= new Square();
		
		double result=sq.square(radious); 

		
		return result*3.14;
	}
	
}

public class Aggeration {
public static void main(String[]args) {
	Circle obj = new Circle();
	System.out.println(obj.circleArea(5.6));
}
}

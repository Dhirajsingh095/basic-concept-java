package BasicConcept;

import java.util.Scanner;

public class TypeCast {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int i = sc.nextInt();
		int j = sc.nextInt();
		System.out.println("Enter no:");
		float num = sc.nextFloat();
		int x = (int) num;
		int result = i + j;

		System.out.println(result);
		System.out.println(x);
		sc.close();

	}
}

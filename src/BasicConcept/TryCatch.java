package BasicConcept;

class App {

	int nominator = 100;
	int denominator[] = { 2, 5, 6, 0, 12 };
	int result = 0;

	public void tryCatch() {

		for (int i = 0; i < 5; i++) {
			try {

				result = nominator / denominator[i];
				System.out.println(result);
			} catch (Exception e) {
				System.out.println(e);
			}
		}

	}
}

public class TryCatch {

	public static void main(String[] args) {

		App obj = new App();
		obj.tryCatch();

	}

}
